CREATE OR REPLACE FUNCTION add_fbt_recom()
	RETURNS TRIGGER AS $$
	DECLARE p1 INTEGER;
	DECLARE p2 INTEGER;
	DECLARE recom Record;
	BEGIN
	IF NEW.payed = FAlsE THEN
		RETURN NULL;
	END IF;

--	product_ids := (select item.product_id from item where id IN (SELECT item_id FROM sale_item where sale_item.sale_order_id = NEW.id));
	
	FOR p1 in
		select item.product_id
		from item
		where id IN (SELECT item_id FROM sale_item where sale_item.sale_order_id = NEW.id)
	LOOP
		FOR p2 in select item.product_id from item where item.id IN (SELECT item_id FROM sale_item where sale_item.sale_order_id = NEW.id)
		LOOP
		--raise notice 'hello' ;
			IF p1 >= p2 THEN
				CONTINUE;
			END if;
			SELECT * INTO recom from fbt_recommendation FBT where FBT.product_id_1 = p1 and FBT.product_id_2 = p2;
			IF FOUND THEN
				UPDATE fbt_recommendation
				SET quantity = quantity + 1;
			ELSE
				INSERT INTO fbt_recommendation VALUES(p1, p2, 1);
			END IF;
		END LOOP;
	END LOOP;
	RETURN NULL;
	END;
$$ LANGUAGE PLpgSQL;

/*
CREATE OR REPLACE FUNCTION add_fbt_recom()
	RETURNS TRIGGER AS $$
BEGIN
	IF OLD.payed <> FALSE OR NEW.payed <> TRUE THEN
		RETURN NULL;
	END IF;

	FOR p1 in
		select item.product_id
		from item
		where id IN (SELECT item_id FROM sale_item where sale_item.sale_order_id = NEW.id)
	LOOP
		raise NOTICE p1.name
	RAISE EXCEPTION 'error';
END
$$ LANGUAGE PLpgSQL;
*/

DROP TRIGGER IF EXISTS  add_fbt_recom ON sale_order;
CREATE TRIGGER add_fbt_recom
	AFTER UPDATE ON sale_order
	FOR EACH ROW
	EXECUTE PROCEDURE add_fbt_recom();


UPDATE sale_order
--SET payed = FALSE
SET payed = TRUE
WHERE id = 1;
	 




