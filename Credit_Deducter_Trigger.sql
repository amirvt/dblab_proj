CREATE OR REPLACE FUNCTION credit_deducter()
	RETURNS TRIGGER AS $$
	DECLARE _sale_item Record;
	DECLARE buyer Record;
	Declare _cost INTEGER;
	Declare _sum INTEGER = 0;
	BEGIN
	IF NEW.payed = FAlsE THEN
		RETURN NULL;
	END IF;

	FOR _sale_item IN
		select *
		from sale_item
		where sale_item.sale_order_id = NEW.id
	LOOP

		select item.price into _cost from item where item.id = _sale_item.item_id;
		_sum := _sum + _cost * _sale_item.quantity;
	END LOOP;



	select * into buyer from client where id = NEW.client_id;



	if buyer.credit - _sum < 0 THEN
		raise EXCEPTION 'Not Enough Credit';
	end if;

	UPDATE client
	SET credit = credit - _sum
	where id = buyer.id;

return null;
	END;
$$ LANGUAGE PLpgSQL;


DROP TRIGGER IF EXISTS credit_deducter ON sale_order;
CREATE TRIGGER credit_deducter
	BEFORE UPDATE ON sale_order
	FOR EACH ROW
	EXECUTE PROCEDURE credit_deducter();


UPDATE sale_order
--SET payed = FALSE
SET payed = true
WHERE id = 1;
	 




