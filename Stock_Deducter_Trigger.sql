CREATE OR REPLACE FUNCTION stock_deductor()
	RETURNS TRIGGER AS $$
	DECLARE _sale_item Record;
	DECLARE prod Record;
	BEGIN
	IF NEW.payed = FAlsE THEN
		RETURN NULL;
	END IF;
	
	FOR _sale_item IN
		select *
		from sale_item
		where sale_item.sale_order_id = NEW.id
	LOOP
		select * into prod from product where id = (select item.product_id from item where item.id = _sale_item.item_id);	
		if prod.quantity - _sale_item.quantity < 0 THEN
			raise	EXCEPTION 'Not Enough of Item in Stock';
		end IF;
		update product
		set quantity = quantity - _sale_item.quantity
		where id = prod.id;	
	END LOOP;
return null;
	END;
$$ LANGUAGE PLpgSQL;


DROP TRIGGER IF EXISTS stock_deductor ON sale_order;
CREATE TRIGGER stock_deductor
	BEFORE UPDATE ON sale_order
	FOR EACH ROW
	EXECUTE PROCEDURE stock_deductor();


UPDATE sale_order
--SET payed = FALSE
SET payed = true
WHERE id = 1;
	 




