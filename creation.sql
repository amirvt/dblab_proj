drop schema public cascade;
create schema public;

create TABLE category
(
	id SERIAL PRIMARY KEY,
	name VARCHAR(255)
);


CREATE TABLE product
(
	id SERIAL PRIMARY KEY,
	name varchar(255) NOT NULL,
	description VARCHAR(1023),
	quantity INTEGER (CHECK quantity >= 0)
);


CREATE TABLE category_product
(
	category_id INTEGER REFERENCES category(id),
	product_id INTEGER REFERENCES product(id),
	PRIMARY KEY (category_id, product_id)
);

CREATE TABLE item
(
	id SERIAL PRIMARY KEY,
	product_id INTEGER REFERENCES product(id),
	info varchar(1023),
	price INTEGER NOT NULL,
	active BOOLEAN NOT NULL
);



CREATE TABLE sale_order
(
	id SERIAL PRIMARY KEY,
	order_date DATE NOT NULL,
	client_id INTEGER REFERENCES client(id),
	payed BOOLEAN NOT NULL
);

CREATE TABLE sale_item
(
	item_id INTEGER REFERENCES item(id),
	sale_order_id INTEGER REFERENCES sale_order(id),
	PRIMARY KEY (item_id, sale_order_id),
	quantity INTEGER NOT NULL CHECK (quantity > 0)
);


-- USERS --

create TABLE client
(
	id SERIAL PRIMARY key,
	email VARCHAR(255) NOT NULL UNIQUE,
	credit INT DEFAULT 0
);

-- RECOMMENDATIONS --

CREATE TABLE fbt_recommendation
(
	product_id_1 INTEGER REFERENCES product(id),
	product_id_2 INTEGER REFERENCES product(id),
	quantity INTEGER NOT NULL CHECK (quantity > 0)
);

-- ROLES --
/*
create table admin_roles
(
	id INTEGER REFERENCES admin_user(id),
	role_name VARCHAR(255) NOT NULL
);

create TABLE admin_user
(
	id SERIAL PRIMARY KEY,
	email VARCHAR(255) NOT NULL UNIQUE
);
*/